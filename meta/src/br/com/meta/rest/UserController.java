package br.com.meta.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.meta.entity.User;
import br.com.meta.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService service;
	
	final static Logger LOGGER = Logger.getLogger(UserController.class);

	/**
	 * 
	 * get a user
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getByID(@PathVariable("id") Integer id) {
		User user = null;
		try {
			user = service.getByID(id);
			if (user == null) {
				return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * update user
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public ResponseEntity<User> update(@RequestBody User user) {
		try {
			return new ResponseEntity<User>(service.saveOrUpdate(user), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * save user
	 * 
	 * @param user
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<User> save(@RequestBody User user) {
		try {
			return new ResponseEntity<User>(service.saveOrUpdate(user), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * remove user
	 * 
	 * @param id
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeByID(@PathVariable("id") Integer id) {
		try {
			service.removeByID(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
