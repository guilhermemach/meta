package br.com.meta.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.util.Arrays;

import br.com.meta.Main;
import br.com.meta.entity.Departament;
import br.com.meta.entity.Permission;
import br.com.meta.entity.User;
import br.com.meta.repository.UserRepository;

import java.nio.charset.Charset;

import org.springframework.http.MediaType;

/**
 * 
 * @author guimachado
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class UserControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private UserRepository repository;

	private User userLocal = null;

	private static final MediaType JSON_MEDIA_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		User user = new User("teste", "teste");
		user.setDepartament(new Departament("teste", "teste"));
		user.setPermissions(Arrays.asList(new Permission("teste", "teste")));
		this.userLocal = repository.save(user);
	}

	@Test
	public void findByIdTest() throws Exception {
		mockMvc.perform(get("/user/" + userLocal.getId()).contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.id", is(userLocal.getId())))
				.andExpect(jsonPath("$.name", is(userLocal.getName())))
				.andExpect(jsonPath("$.departament.name", is(userLocal.getDepartament().getName())))
				.andExpect(jsonPath("$.permissions[0].name", is(userLocal.getPermissions().get(0).getName())))
				.andExpect(status().isOk());
	}

	@Test
	public void updateTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		userLocal.setName("changed");
		String json = mapper.writeValueAsString(userLocal);

		mockMvc.perform(put("/user").content(json).contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.name", is(userLocal.getName()))).andExpect(status().isOk());
	}

	@Test
	public void deleteByIdTest() throws Exception {
		mockMvc.perform(delete("/user/" + userLocal.getId()).contentType(JSON_MEDIA_TYPE)).andExpect(status().isOk());
	}

	@Test
	public void saveTest() throws Exception {
		User user = new User("teste2", "teste2");
		user.setDepartament(new Departament("teste2", "teste2"));
		user.setPermissions(Arrays.asList(new Permission("teste2", "teste2")));

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(user);

		mockMvc.perform(post("/user/").content(json).contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.name", is(user.getName())))
				.andExpect(jsonPath("$.departament.name", is(user.getDepartament().getName())))
				.andExpect(jsonPath("$.permissions[0].name", is(user.getPermissions().get(0).getName())))
				.andExpect(status().isOk());
	}
	
	@Test
	public void saveServerErrorTest() throws Exception {
		User user = new User("teste2", "teste2");
		Departament dep = new Departament("teste2", "teste2");
		dep.setId(3); // constraint
		user.setDepartament(dep);
		user.setPermissions(Arrays.asList(new Permission("teste2", "teste2")));

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(user);

		mockMvc.perform(post("/user/").content(json).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().is5xxServerError());
	}


	@Test
	public void findByIdNotFoundTest() throws Exception {
		mockMvc.perform(get("/user/0").contentType(JSON_MEDIA_TYPE)).andExpect(status().isNotFound());
	}

	@After
	public void clearup() throws Exception {
		repository.deleteAll();
	}

}
