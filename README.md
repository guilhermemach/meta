# README #


### Test Meta ###

# Summary

The application is a standalone solution for purpose of tests (using spring boot) with an embbeded database.


# Application Libraries/Framework

* Java8
* Maven3.3
* Spring Boot with Spring MVC
* Hibernate/JPA
* MockMvc

# Tools

* Eclipse
* RestClient (http rest client)

# Deploy / Run

Necessary to have Maven to install lib dependencies e run the project.

```
#!linux

$ mvn install
$ mvn test

```


Deployment instructions


```
#!linux

$ mvn spring-boot:run

```


# HTTP Queries

* http://localhost:8080/user (use the http operations to hit each method)

json sample


* POST - http://localhost:8080/user

```
#!json
{"name":"teste" , "description": "teste",  "departament": {"name":"teste", "description":"description"}, "permissions":[{"name":"pteste", "description":"description"}] }
```


* GET - http://localhost:8080/user/{id}

```
#!json
{"id":5,"name":"teste","description":"teste","departament":{"id":5,"name":"teste","description":"description"},"permissions":[{"id":3,"name":"pteste","description":"description"}]}
```


* PUT - http://localhost:8080/user (send the json with id's)

* DELETE - http://localhost:8080/user/{id}