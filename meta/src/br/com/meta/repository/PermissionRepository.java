package br.com.meta.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.meta.entity.Permission;


public interface PermissionRepository extends CrudRepository<Permission, Long> {

}
