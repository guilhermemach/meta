package br.com.meta.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.meta.Main;
import br.com.meta.entity.Departament;
import br.com.meta.entity.Permission;
import br.com.meta.entity.User;
import br.com.meta.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
public class UserRepositoryTest {

	@Autowired
	private UserRepository repository;

	private User localUser = null;

	@Before
	public void setup() throws Exception {
		User user = new User("teste", "teste");
		user.setDepartament(new Departament("teste", "teste"));
		user.setPermissions(Arrays.asList(new Permission("teste", "teste")));
		this.localUser = this.repository.save(user);
	}

	@Test
	public void findAllTest() throws Exception {
		List<User> list = (List<User>) this.repository.findAll();
		assertEquals(1, list.size());
	}

	@Test
	public void findByIdTest() throws Exception {
		User user = repository.findOne(localUser.getId());
		assertNotNull(user);
	}

	@Test
	public void saveTest() throws Exception {
		User user = new User("teste", "teste");
		user.setDepartament(new Departament("teste", "teste"));
		user.setPermissions(Arrays.asList(new Permission("teste", "teste")));
		User response = repository.save(user);
		assertTrue(response.getId() > 0);
	}

	@Test
	public void updateTest() throws Exception {		
		String oldvar = localUser.getName();		
		this.localUser.setName("changed");
		User response = repository.save(this.localUser);
		assertNotEquals(oldvar , response.getName());
	}
	
	@Test
	public void removeTest() throws Exception {		
		repository.delete(this.localUser.getId());
		User response = repository.findOne(this.localUser.getId());
		assertNull(response);
	}

	@After
	public void clearup() throws Exception {
		this.repository.deleteAll();
	}

}
