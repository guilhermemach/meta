package br.com.meta.repository;

import org.springframework.data.repository.CrudRepository;
import br.com.meta.entity.Departament;


public interface DepartamentRepository extends CrudRepository<Departament, Integer> {

}
