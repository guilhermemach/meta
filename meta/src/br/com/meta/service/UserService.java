package br.com.meta.service;

import java.util.List;

import br.com.meta.entity.User;

public interface UserService {
	
	public List<User> getAll();

	public User getByID(Integer id);

	public void removeByID(Integer id);

	public User saveOrUpdate(User obj);

}
