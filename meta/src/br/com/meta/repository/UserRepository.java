package br.com.meta.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.meta.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	
}
