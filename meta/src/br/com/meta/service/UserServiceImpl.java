package br.com.meta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meta.entity.User;
import br.com.meta.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repository;
	
	@Override
	public List<User> getAll() {		
		return (List<User>) repository.findAll();
	}

	@Override
	public User getByID(Integer id) {	
		return repository.findOne(id);
	}

	@Override
	public void removeByID(Integer id) {
		repository.delete(id);		
	}

	@Override
	public User saveOrUpdate(User user) {	
		
		return repository.save(user);
	}

}
