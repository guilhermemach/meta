package br.com.meta;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * startup spring boot application.
 * 
 * @author guimachado
 *
 */

@SpringBootApplication
public class Main {
 
  public static void main(String[] args) throws Exception {
    SpringApplication.run(new Object[] {Main.class}, args);
  }
  
 
}

